"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.files = void 0;
const os = require('os');
const files = (filename) => {
    const body = "import {connect} from 'react-redux'" + os.EOL +
        "import { View } from '@tarojs/components';" + os.EOL +
        "import './" + filename + ".less';" + os.EOL +
        "" + os.EOL +
        "const Index:React.FC=(props:any)=>{" + os.EOL +
        "  return(" + os.EOL +
        "    <View className='index'>" + os.EOL +
        "      page" + os.EOL +
        "    </View>" + os.EOL +
        "  );" + os.EOL +
        "};" + os.EOL + os.EOL + os.EOL +
        "export default connect (({" + filename + "}:any)=>({..." + filename + "}))(Index);";
    const model = "export default{" + os.EOL +
        "  namespace:'" + filename + "'," + os.EOL +
        "  state:{" + os.EOL + os.EOL +
        "  }," + os.EOL +
        "  effects:{" + os.EOL + os.EOL +
        "  }," + os.EOL +
        "  reducers:{" + os.EOL +
        "    save(state,{payload}){" + os.EOL +
        "         return {...state,...payload}" + os.EOL +
        "    }" + os.EOL +
        "  }" + os.EOL +
        "}";
    let page = {
        "tsx": body,
        "ts": model,
        "less": "",
        "config.ts": "export default { navigationBarTitleText: '' };"
    };
    let components = {
        "tsx": body,
        "less": "",
    };
    return {
        page,
        components
    };
};
exports.files = files;
//# sourceMappingURL=files.js.map