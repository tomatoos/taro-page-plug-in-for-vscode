"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.files = void 0;
const fs = require("fs");
const path = require("path");
const files = (filename) => {
    console.log('%cindex.ts line:5 fs.read', 'color: #007acc;', fs.readFileSync(path.join(__dirname, 'page.tsx')));
    let page = {
        "tsx": "import { View } from '@tarojs/components';" +
            "import './index.less';" +
            "const Index:React.FC=()=>{" +
            "return(" +
            " <View className='index'>" +
            "page" +
            "</View>" +
            ");" +
            "};" +
            "export default Index;",
        "less": "",
        "config.ts": "export default {" +
            "navigationBarTitleText: '首页'" +
            "};"
    };
    let components = {
        "tsx": "import { View } from '@tarojs/components';" +
            "import './index.less';" +
            "const Index:React.FC=()=>{" +
            "return(" +
            " <View className='index'>" +
            "page" +
            "</View>" +
            ");" +
            "};" +
            "export default Index;",
        "less": "",
    };
    return {
        page,
        components
    };
};
exports.files = files;
//# sourceMappingURL=index.js.map