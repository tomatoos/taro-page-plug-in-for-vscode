"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const vscode = require("vscode");
const fs = require("fs");
const path = require("path");
const files_1 = require("./files");
const fileTypes = ['config.ts', 'tsx', 'less', 'ts'];
function wxCommandHander(type, e) {
    vscode.window.showInputBox({
        placeHolder: "enter title",
    })
        .then((title) => {
        if (!/^\w+$/.test(title)) {
            vscode.window.showInformationMessage("文件名只能是数字、字母或_组合");
            return;
        }
        var dir = e.fsPath + "/" + title;
        if (fs.existsSync(dir)) {
            vscode.window.showInformationMessage("content already exist");
            return;
        }
        fs.mkdirSync(dir);
        console.log('%cextension.ts line:34 title', 'color: #007acc;', title);
        /**
         * 文件路径
         * dir + "/title.tsx";
         */
        /**
         * path.sep
         * 提供平台特定的路径片段分隔符：
         * Windows 上是 \
         * POSIX 上是 /
         */
        // const filename = String(dir).split(path.sep).pop();
        const filename = title;
        const { page, components } = files_1.files(`${filename}`);
        const stat = fs.statSync(e.fsPath);
        //获取pages上一级文件路径
        const dirParent = e.fsPath.replace(path.join('/pages'), '');
        console.log('%cextension.ts line:43 filename', 'color: #007acc;', filename);
        let models = fs.readFileSync(path.join(dirParent + '/models/index.ts'), 'utf8').split('\n');
        models = Array.from(new Set(models));
        models = models.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, ""); });
        let modelsIndex = models.findIndex(function (value) { return value.indexOf('export') >= 0; });
        let modelsText1 = "import " + filename + " from '../pages/" + filename + "/model'";
        let modelsText2 = filename;
        models.splice(modelsIndex, 0, modelsText1);
        models.splice(modelsIndex + 1, 0, "");
        models.splice(modelsIndex + 1, 0, "");
        models.splice(modelsIndex + 1, 0, "");
        models[models.length - 2] = models[models.length - 2] + ',';
        models.splice(models.length - 1, 0, modelsText2);
        fs.writeFileSync(path.join(`${dirParent}/models/index.ts`), models.join('\n'));
        //配置文件
        let config = fs.readFileSync(path.join(`${dirParent}/app.config.ts`), 'utf-8').split('\n');
        //过滤换行符
        config = config.filter(function (e) { return e.replace(/(\r\n|\n|\r)/gm, ""); });
        let pageIndex = config.findIndex(function (value) { return value.indexOf(']') >= 0; });
        config[pageIndex - 1] = config[pageIndex - 1] + ',';
        const configText = "pages/" + filename + "/" + filename;
        config.splice(pageIndex, 0, "'" + configText + "'");
        fs.writeFileSync(path.join(`${dirParent}/app.config.ts`), config.join('\n'));
        if (stat.isDirectory()) {
            try {
                fileTypes.map((i) => __awaiter(this, void 0, void 0, function* () {
                    const data = new Uint8Array(Buffer.from(type === 'page' ? page[i] : components[i]));
                    if (i === 'ts') {
                        fs.writeFileSync(path.join(`${dir}/model.${i}`), data);
                    }
                    else {
                        fs.writeFileSync(path.join(`${dir}/${filename}.${i}`), data);
                    }
                }));
                vscode.window.showInformationMessage(`create ${type} success!`);
            }
            catch (error) {
                vscode.window.showErrorMessage('create page files failed');
            }
        }
        else {
            vscode.window.showErrorMessage('please choose a folder');
        }
    });
}
function activate(context) {
    console.log('Congratulations, your extension "create-taro-page" is now active!');
    let disposable = vscode.commands.registerCommand('create-taro-page.createTaroPage', (e) => __awaiter(this, void 0, void 0, function* () {
        wxCommandHander('page', e);
    }));
    context.subscriptions.push(disposable);
    vscode.commands.registerCommand('create-taro-page.createTaroComponents', (e) => __awaiter(this, void 0, void 0, function* () {
        wxCommandHander('components', e);
    }));
    context.subscriptions.push(disposable);
}
exports.activate = activate;
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map