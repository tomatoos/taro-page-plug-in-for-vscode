const os = require('os');

const files=(filename:string)=>{

  const body="import {connect} from 'react-redux'"+os.EOL+
  "import { View } from '@tarojs/components';"+os.EOL+
  "import './"+filename+".less';"+os.EOL+
  ""+os.EOL+
  "const Index:React.FC=(props:any)=>{"+os.EOL+
  "  return("+os.EOL+
    "    <View className='index'>"+os.EOL+
    "      page"  +os.EOL+
    "    </View>"+os.EOL+
  "  );"+os.EOL+
  "};"+os.EOL+os.EOL+os.EOL+
  "export default connect (({"+filename+"}:any)=>({..."+filename+"}))(Index);";


  const model="export default{"+os.EOL+
    "  namespace:'"+filename+"',"+os.EOL+
    "  state:{"+os.EOL+os.EOL+

    "  },"+os.EOL+
    "  effects:{"+os.EOL+os.EOL+

    "  },"+os.EOL+
    "  reducers:{"+os.EOL+
    "    save(state,{payload}){"+os.EOL+
    "         return {...state,...payload}"+os.EOL+
    "    }"+os.EOL+
    "  }"+os.EOL+

    "}";


  let page:any={
    "tsx":body,
    "ts":model,
    "less":"",
    "config.ts":"export default { navigationBarTitleText: '' };"
  };

  let components:any={
    "tsx":body,
    "less":"",
  };


  return{
    page,
    components
  };

};
export {files};