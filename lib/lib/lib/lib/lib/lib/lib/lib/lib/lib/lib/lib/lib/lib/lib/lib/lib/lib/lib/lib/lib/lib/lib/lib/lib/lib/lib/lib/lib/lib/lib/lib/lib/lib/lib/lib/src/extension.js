"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const fs = require("fs");
const path = require("path");
const files_1 = require("./files");
const fileTypes = ['config.ts', 'tsx', 'less'];
function wxCommandHander(type, e) {
    const stat = fs.statSync(e.fsPath);
    const dir = path.normalize(e.fsPath);
    vscode.window.showInformationMessage(`stat是${stat}!`);
    const filename = String(dir).split("/").pop();
    vscode.window.showInformationMessage(`filename是${filename}!`);
    const { page, components } = files_1.files(`${filename}`);
    if (stat.isDirectory()) {
        try {
            fileTypes.map(async (i) => {
                const data = new Uint8Array(Buffer.from(type === 'page' ? page[i] : components[i]));
                fs.writeFileSync(`${dir}/${filename}.${i}`, data);
            });
            vscode.window.showInformationMessage(`create ${type} success!`);
        }
        catch (error) {
            vscode.window.showErrorMessage('create page files failed');
        }
    }
    else {
        vscode.window.showErrorMessage('please choose a folder');
    }
}
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "create-taro-page" is now active!');
    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('create-taro-page.createTaroPage', async (e) => {
        // The code you place here will be executed every time your command is executed
        // Display a message box to the user
        wxCommandHander('page', e);
    });
    context.subscriptions.push(disposable);
    vscode.commands.registerCommand('create-taro-page.createTaroComponents', async (e) => {
        wxCommandHander('components', e);
    });
    context.subscriptions.push(disposable);
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map