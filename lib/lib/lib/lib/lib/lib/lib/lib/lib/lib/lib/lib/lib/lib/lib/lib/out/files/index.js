"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.files = void 0;
const fs = require("fs");
const path = require("path");
const files = (filename) => {
    const page = {
        "tsx": fs.readFileSync(path.join(__dirname, '/page/index.tsx')),
        "less": fs.readFileSync(path.join(__dirname, '/page/index.less')),
        "config.ts": fs.readFileSync(path.join(__dirname, '/page/index.config.ts'))
    };
    const components = {
        "tsx": fs.readFileSync(path.join(__dirname, '/components/index.tsx')),
        "less": fs.readFileSync(path.join(__dirname, '/components/index.less')),
    };
    return {
        page,
        components
    };
};
exports.files = files;
//# sourceMappingURL=index.js.map